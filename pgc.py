#from icalendar import Calendar, Event
from ics import Calendar, Event
from datetime import datetime
from flask import Flask, request, send_file
import json
app = Flask(__name__)
app.config['JSON_AS_ASCII'] = False

#Documentation
@app.route('/')
def hello_world():
    return 'Hello World!'

#Afficher tous les cours
@app.route('/courses')
def display_courses():
    with open("courses.json", "r", encoding="utf8") as outfile:
        filedata = json.load(outfile)
        return filedata

#Recherche par cours: de l'autre côté il faut prendre le code du cours et le stocker dans un buffer "Sélection" pour ensuite les POST pour en faire un fichier ICS
@app.route('/courses/<courseCode>')
def search_course(courseCode):
    courses = dict({})
    with open("courses.json", "r", encoding="utf8") as outfile:
        filedata = json.load(outfile)
        for lists in filedata["courses"]:
            for key, value in lists.items():
                if (key == "code" and value == courseCode):
                    courses["Code"] = value
                    courses["Titre"] = lists["title"]
                    courses["Annee"] = lists["academicalYear"]
                    courses["Faculte"] = lists["facultyLabel"]
                    courses["Niveau"] = lists["studyLevel"]
                    courses["Type"] = lists["type"]
                    courses["Duree"] = lists["duration"]
                    courses["Periode"] = lists["periodicity"]
                    courses["Langue"] = lists["language"]
                    if(lists.get("day") != None):
                        courses["Jour"] = lists["day"]
                    if(lists.get("startHour") != None):
                        courses["heureDebut"] = lists["startHour"]
                    if(lists.get("endHour") != None):
                        courses["heureFin"] = lists["endHour"]
                    if(lists.get("building") != None):
                        courses["Lieu"] = lists["building"] 
                    if(lists.get("room") != None):
                        courses["Salle"] = lists["room"]  
                    if(lists.get("displayFirstName") != None and lists.get("displayLastName") != None):
                        courses["Enseignant"] = (lists["displayFirstName"] + " " + lists["displayLastName"])                                                                                                                    
    return courses

@app.route('/faculties')
def display_faculties():
    coursesGroupedByFacultyID = {}
    facultyIdSet = set({})
    with open("courses.json", "r", encoding="utf8") as outfile:
        filedata = json.load(outfile)
        for lists in filedata["courses"]:
            for key, value in lists.items():
                if (key == "facultyId"):
                    facultyID = value
                    code = lists["code"]
                    label = lists["facultyLabel"]                    
                    if (facultyID not in facultyIdSet):
                        facultyIdSet.add(facultyID)                                   
                        coursesGroupedByFacultyID[facultyID] = {label: []}
                    else:
                        for k, v in coursesGroupedByFacultyID[facultyID].items():
                            if code not in set(v):
                                v.append(code)
    return coursesGroupedByFacultyID

@app.route('/faculties/<facultyId>')
def search_faculty_courses(facultyId):
    facultyIDCourses = []
    coursesGroupedByFacultyID = display_faculties()
    for key, value in coursesGroupedByFacultyID.items():
        if key == facultyId:
            for k, v in value.items():
                for course in v:
                    info = search_course(course)                    
                    facultyIDCourses.append(info)
    return facultyIDCourses

#Only used to create other functions
@app.route('/studyplans')
def display_studyplansCourses():
    coursesGroupedByStudyPlanID = {}
    with open("courses.json", "r", encoding="utf8") as outfile:
        filedata = json.load(outfile)
        for lists in filedata["courses"]:
            for key, value in lists.items():
                if (key == "code"):
                    code = value
                if (key == "listStudyPlan"):
                    if(lists.get("listStudyPlan") != None):
                        for arrays in value:
                            for k, v in arrays.items():
                                if (k == "studyPlanId"):
                                    if(arrays.get("studyPlanId") != None):
                                        if v in coursesGroupedByStudyPlanID:
                                            coursesGroupedByStudyPlanID[v].append(code)
                                        else:
                                            coursesGroupedByStudyPlanID[v] = [code]
    return coursesGroupedByStudyPlanID

#Only used to create other functions
@app.route('/studyplansGroups')
def display_studyplansGroupsCourses():
    coursesGroupedByStudyPlanGroupID = {}
    with open("courses.json", "r", encoding="utf8") as outfile:
        filedata = json.load(outfile)
        for lists in filedata["courses"]:
            for key, value in lists.items():
                if (key == "code"):
                    code = value
                if (key == "listStudyPlan"):
                    for arrays in value:
                        for k, v in arrays.items():
                            if (k == "studyPlanGroupId"):
                                if v in coursesGroupedByStudyPlanGroupID:
                                    coursesGroupedByStudyPlanGroupID[v].append(code)
                                else:
                                    coursesGroupedByStudyPlanGroupID[v] = [code]
    return coursesGroupedByStudyPlanGroupID

@app.route('/facultyStudyplans')
def display_facultyStudyplans():
    studyPlansIDGroupedByFacultyID = {}
    facultyIdSet = set({})
    studyPlanSet = set ({})
    with open("courses.json", "r", encoding="utf8") as outfile:
        filedata = json.load(outfile)
        for lists in filedata["courses"]:
            for key, value in lists.items():
                if (key == "facultyId"):
                    facultyID = value
                    label = lists["facultyLabel"]     
                    for arrays in lists["listStudyPlan"]:
                        for k, v in arrays.items():
                            if (k == "studyPlanId"):
                                studyPlanID = v
                                if(arrays["facultyId"] == facultyID):                            
                                    if (facultyID not in facultyIdSet):
                                        facultyIdSet.add(facultyID)                                   
                                        studyPlansIDGroupedByFacultyID[facultyID] = {label: [dict()]}
                                    else:
                                        for k1, v1 in studyPlansIDGroupedByFacultyID[facultyID].items():
                                            for dicts in v1:
                                                if (dicts == {}):
                                                    v1.remove(dicts)
                                                if studyPlanID not in studyPlanSet:
                                                    studyPlanSet.add(studyPlanID)
                                                    v1.append({studyPlanID: arrays["studyPlanLabel"]})                                  
    return studyPlansIDGroupedByFacultyID

#@app.route('/studyplans/<facultyId>')
#def search_facultyStudyplans(facultyId):
#    studyPlansIDGroupedByFacultyID = display_studyplans()
#    facultyStudyPlans = []
#    for key, value in studyPlansIDGroupedByFacultyID.items():
#        if key == facultyId:
#            facultyStudyPlans.append(value)
#    return facultyStudyPlans

@app.route('/facultyStudyplans/<facultyId>')
def search_facultyStudyplans(facultyId):
    studyPlansIDGroupedByFacultyID = display_facultyStudyplans()
    #facultyStudyPlans = []
    for key, value in studyPlansIDGroupedByFacultyID.items():
        if key == facultyId:
            for k, v in value.items():
                facultyStudyPlans = v
    return facultyStudyPlans

@app.route('/facultyStudyplansDetailed/<facultyId>')
def facultyStudyplansDetailed(facultyId):
    res = []
    studyPlanIds = search_facultyStudyplans(facultyId)
    for l in studyPlanIds:
        for cle, val in l.items():
            studyPlanGroupIDsOfStudyPlanIDs = {}
            coursesGroupedByStudyPlanID = display_studyplansCourses()
            for key, value in coursesGroupedByStudyPlanID.items(): #iterating on the key/values of the ONLY dict JSONobject contained within studyPlans.json
                if (key == cle):
                    studyPlanID = key
                    for courses in value: #iterating on all the codes contained within the Lists of each value
                        with open("courses.json", "r", encoding="utf8") as outfile:
                            filedata = json.load(outfile)
                            for lists in filedata["courses"]:                                         
                                for key1, value1 in lists.items():
                                    if (key1 == "code" and value1 == courses):
                                        code = value1
                                        for arrays in lists["listStudyPlan"]:
                                            for k, v in arrays.items():
                                                if (k == "studyPlanId" and v == studyPlanID):
                                                    studyPlanGroupID = arrays["studyPlanGroupId"]
                                                    coursesGroupedByStudyPlanGroupID = display_studyplansGroupsCourses()
                                                    for k1, v1 in coursesGroupedByStudyPlanGroupID.items():
                                                        if(k1 == studyPlanGroupID):                                                        
                                                            for codes in v1:                                                                                                                  
                                                                if v in studyPlanGroupIDsOfStudyPlanIDs:
                                                                    for dicts in studyPlanGroupIDsOfStudyPlanIDs[v]:                                                          
                                                                        if studyPlanGroupID in dicts:
                                                                            if codes not in set(dicts[studyPlanGroupID]):
                                                                                dicts[studyPlanGroupID].append(codes)
                                                                                #res2.append(search_course(codes))
                                                                        else:
                                                                            dicts[studyPlanGroupID] = [codes]
                                                                else:
                                                                    studyPlanGroupIDsOfStudyPlanIDs[v] = [{studyPlanGroupID: [codes]}]
        res.append(studyPlanGroupIDsOfStudyPlanIDs)
    return res

#create another route, that will enable to display course names from StudyPlanGroups of StudyPlan

@app.route('/convertCourseToEvent/<code>')
def convert(code):
    event = Event()
    info = search_course(code)
    if (info.get("heureDebut") != None):
        if (info["heureDebut"] < 10):
            info["heureDebut"] = "0" + str(info["heureDebut"])
    if (info.get("heureFin") != None):
        if (info["heureFin"] < 10):
            info["heureFin"] = "0" + str(info["heureFin"])
    today = datetime.now()
    date = today.strftime("%d/%m/%Y")
    ddmmyy = date.split("/")
    if(info.get("Periode") != None):
            if(info["Periode"] == "Automne"):
                if(info.get("Jour") != None and info.get("heureDebut") != None and info.get("heureFin") != None):
                    if(info["Jour"] == "Lundi"):
                        event.begin = str(ddmmyy[2]) + "-" + "09" + "-" + "01" + " " + str(info["heureDebut"]) + ":00:00"
                        event.end = str(ddmmyy[2]) + "-" + "09" + "-" + "01" + " " + str(info["heureFin"]) + ":00:00"
                    if(info["Jour"] == "Mardi"):
                        event.begin = str(ddmmyy[2]) + "-" + "09" + "-" + "02" + " " + str(info["heureDebut"]) + ":00:00"
                        event.end = str(ddmmyy[2]) + "-" + "09" + "-" + "02" + " " + str(info["heureFin"]) + ":00:00"
                    if(info["Jour"] == "Mercredi"):
                        event.begin = str(ddmmyy[2]) + "-" + "09" + "-" + "03" + " " + str(info["heureDebut"]) + ":00:00"
                        event.end = str(ddmmyy[2]) + "-" + "09" + "-" + "03" + " " + str(info["heureFin"]) + ":00:00"
                    if(info["Jour"] == "Jeudi"):
                        event.begin = str(ddmmyy[2]) + "-" + "09" + "-" + "04" + " " + str(info["heureDebut"]) + ":00:00"
                        event.end = str(ddmmyy[2]) + "-" + "09" + "-" + "04" + " " + str(info["heureFin"]) + ":00:00"
                    if(info["Jour"] == "Vendredi"):
                        event.begin = str(ddmmyy[2]) + "-" + "09" + "-" + "05" + " " + str(info["heureDebut"]) + ":00:00"
                        event.end = str(ddmmyy[2]) + "-" + "09" + "-" + "05" + " " + str(info["heureFin"]) + ":00:00"
                    if(info["Jour"] == "Samedi"):
                        event.begin = str(ddmmyy[2]) + "-" + "09" + "-" + "06" + " " + str(info["heureDebut"]) + ":00:00"
                        event.end = str(ddmmyy[2]) + "-" + "09" + "-" + "06" + " " + str(info["heureFin"]) + ":00:00"
                    if(info["Jour"] == "Dimanche"):
                        event.begin = str(ddmmyy[2]) + "-" + "09" + "-" + "07" + " " + str(info["heureDebut"]) + ":00:00"
                        event.end = str(ddmmyy[2]) + "-" + "09" + "-" + "07" + " " + str(info["heureFin"]) + ":00:00"
            if(info["Periode"] == "Printemps"):
                if(info.get("Jour") != None and info.get("heureDebut") != None and info.get("heureFin") != None):
                    if(info["Jour"] == "Lundi"):
                        event.begin = str(int(ddmmyy[2])+1) + "-" + "02" + "-" + "01" + " " + str(info["heureDebut"]) + ":00:00"
                        event.end = str(int(ddmmyy[2])+1) + "-" + "02" + "-" + "01" + " " + str(info["heureFin"]) + ":00:00"
                    if(info["Jour"] == "Mardi"):
                        event.begin = str(int(ddmmyy[2])+1) + "-" + "02" + "-" + "02" + " " + str(info["heureDebut"]) + ":00:00"
                        event.end = str(int(ddmmyy[2])+1) + "-" + "02" + "-" + "02" + " " + str(info["heureFin"]) + ":00:00"
                    if(info["Jour"] == "Mercredi"):
                        event.begin = str(int(ddmmyy[2])+1) + "-" + "02" + "-" + "03" + " " + str(info["heureDebut"]) + ":00:00"
                        event.end = str(int(ddmmyy[2])+1) + "-" + "02" + "-" + "03" + " " + str(info["heureFin"]) + ":00:00"
                    if(info["Jour"] == "Jeudi"):
                        event.begin = str(int(ddmmyy[2])+1) + "-" + "02" + "-" + "04" + " " + str(info["heureDebut"]) + ":00:00"
                        event.end = str(int(ddmmyy[2])+1) + "-" + "02" + "-" + "04" + " " + str(info["heureFin"]) + ":00:00"
                    if(info["Jour"] == "Vendredi"):
                        event.begin = str(int(ddmmyy[2])+1) + "-" + "02" + "-" + "05" + " " + str(info["heureDebut"]) + ":00:00"
                        event.end = str(int(ddmmyy[2])+1) + "-" + "02" + "-" + "05" + " " + str(info["heureFin"]) + ":00:00"
                    if(info["Jour"] == "Samedi"):
                        event.begin = str(int(ddmmyy[2])+1) + "-" + "02" + "-" + "06" + " " + str(info["heureDebut"]) + ":00:00"
                        event.end = str(int(ddmmyy[2])+1) + "-" + "02" + "-" + "06" + " " + str(info["heureFin"]) + ":00:00"
                    if(info["Jour"] == "Dimanche"):
                        event.begin = str(int(ddmmyy[2])+1) + "-" + "02" + "-" + "07" + " " + str(info["heureDebut"]) + ":00:00"
                        event.end = str(int(ddmmyy[2])+1) + "-" + "02" + "-" + "07" + " " + str(info["heureFin"]) + ":00:00"
    if(info.get("Titre") != None):
        event.name = info["Titre"]
    if(info.get("Lieu") != None and info.get("Salle") != None):
        event.location = (info["Lieu"] + " " + info["Salle"])
    else:
        event.location = "Se déroule sur zoom"
    return event

@app.route('/createCalendar', methods=['POST'])    #{"selection": [code1,code2,code3,]}
def createcalendar():
    request_json = request.get_json()
    courses = request_json.get('selection')
    cal = Calendar()
    for course in courses:
        event = convert(course)
        cal.events.add(event)
    with open('schedule.ics', 'w') as schedule:
        #contents = schedule.read().split("\n")
        schedule.seek(0)                      
        schedule.truncate()        
        schedule.writelines(cal.serialize_iter())
    path = 'schedule.ics'
    return send_file(path, as_attachment=True)
        



# Pour le schedule text
#def occShecText(dateTo_find: str, schedule: str) -> int:
#    date = dateTo_find.lower()
#    sche = schedule.lower()
#    for i in range(len(sche)):
#        position = estAlapositionDate(date, i, sche)
#        if position != 0:
#            return position
#    return 0
#
#
#def estAlapositionDate(dateTo_find: str, i: int, schedule: str) -> int:
#    if (len(dateTo_find) + i > len(schedule)):
#        return 0  # Aucune comparaison ne peut être faite.
#    else:
#        # On procède à la comparaison.
#        j = 0
#        while j < len(dateTo_find):
#            if dateTo_find[j] != schedule[i + j]:
#                return 0
#            j += 1
#        # Pour récupérer l'heure directement.
#        return (i + j - 1) + 2


#@app.route('/facultyStudyplans/<facultyId>/<studyPlanId>')
#def search_facultyStudyplans(facultyId, studyPlanId):
#    studyPlansIDGroupedByFacultyID = display_studyplans()
#    #facultyStudyPlans = []
#    result = []
#    for key, value in studyPlansIDGroupedByFacultyID.items():
#        if key == facultyId:
#            for k, v in value.items():
#                facultyStudyPlans = v
#    for dicts in studyPlansIDGroupedByFacultyID:
#        for k1, v1, in dicts.items():
#            result.append(test(studyPlanId))

#@app.route('/test')
#def test():
#    studyPlanGroupIDsOfStudyPlanIDs = {}
#    coursesGroupedByStudyPlanID = display_studyplansCourses()
#    for key, value in coursesGroupedByStudyPlanID.items(): #iterating on the key/values of the ONLY dict JSONobject contained within studyPlans.json
#        if (key == "76640"):
#            studyPlanID = key
#            for courses in value: #iterating on all the codes contained within the Lists of each value
#                with open("courses.json", "r", encoding="utf8") as outfile:
#                    filedata = json.load(outfile)
#                    for lists in filedata["courses"]:                                         
#                        for key1, value1 in lists.items():
#                            if (key1 == "code" and value1 == courses):
#                                code = value1
#                                #print("Value 1: " + value1 + " Courses: " + courses)
#                                for arrays in lists["listStudyPlan"]:
#                                    #print(arrays)
#                                    for k, v in arrays.items():
#                                        if (k == "studyPlanId" and v == studyPlanID):
#                                            studyPlanGroupID = arrays["studyPlanGroupId"]
#                                            #print(studyPlanGroupID)
#                                            coursesGroupedByStudyPlanGroupID = display_studyplansGroupsCourses()
#                                            for k1, v1 in coursesGroupedByStudyPlanGroupID.items():
#                                                if(k1 == studyPlanGroupID):
#                                                    #print(k1)                                                              
#                                                    for codes in v1:
#                                                        #print(codes)                                                                                                                    
#                                                        if v in studyPlanGroupIDsOfStudyPlanIDs:
#                                                            for dicts in studyPlanGroupIDsOfStudyPlanIDs[v]:                                                          
#                                                                if studyPlanGroupID in dicts:
#                                                                    if codes not in set(dicts[studyPlanGroupID]):
#                                                                        dicts[studyPlanGroupID].append(codes)
#                                                                else:
#                                                                    dicts[studyPlanGroupID] = [codes]
#                                                        else:
#                                                            studyPlanGroupIDsOfStudyPlanIDs[v] = [{studyPlanGroupID: [codes]}]
#    return studyPlanGroupIDsOfStudyPlanIDs


## create a new calendar
#cal = Calendar()
#
## create a new event
#event = Event()
#
## set the event's summary
#event.add('summary', 'Weekly Meeting')
#
## set the event's start time and duration
#event.add('dtstart', datetime.datetime(2022, 12, 14, 14, 0, 0))
#event.add('duration', datetime.timedelta(hours=1))
#
## set the event to recur every week on Thursday at 2pm
#event.add('rrule', {'freq': 'weekly', 'byday': 'th'})
#
## add the event to the calendar
#cal.add_component(event)

    #function that prints

#if(info.get("Periode") != None):
#           if(info["Periode"] == "Printemps"):
#               if(info.get("Jour") != None):
#                   if(info["Jour"] == "Lundi"):
#                       if(weekday == "Monday"):
#                           event.begin = ddmmyy[2] + "-" + "09" + "-" + ddmmyy[0] + " " + info["heureDebut"] + ":00:00"
#                       if(weekday == "Tuesday"):
#                           event.begin = ddmmyy[2] + "-" + "09" + "-" + str(int(ddmmyy[0])-1) + " " + info["heureDebut"] + ":00:00"
#                       if(weekday == "Wednesday"):
#                           event.begin = ddmmyy[2] + "-" + "09" + "-" + str(int(ddmmyy[0])-2) + " " + info["heureDebut"] + ":00:00"
#                       if(weekday == "Thursday"):
#                           event.begin = ddmmyy[2] + "-" + "09" + "-" + str(int(ddmmyy[0])-3) + " " + info["heureDebut"] + ":00:00"
#                       if(weekday == "Friday"):
#                           event.begin = ddmmyy[2] + "-" + "09" + "-" + str(int(ddmmyy[0])-4) + " " + info["heureDebut"] + ":00:00"
#                       if(weekday == "Saturday"):
#                           event.begin = ddmmyy[2] + "-" + "09" + "-" + str(int(ddmmyy[0])-5) + " " + info["heureDebut"] + ":00:00"
#                       if(weekday == "Sunday"):
#                   if(info["Jour"] == "Mardi"):
#                       if(weekday == "Monday"):
#                       if(weekday == "Tuesday"):
#                       if(weekday == "Wednesday"):
#                       if(weekday == "Thursday"):
#                       if(weekday == "Friday"):
#                       if(weekday == "Saturday"):
#                       if(weekday == "Sunday"):
#                   if(info["Jour"] == "Mercredi"):
#                       if(weekday == "Monday"):
#                       if(weekday == "Tuesday"):
#                       if(weekday == "Wednesday"):
#                       if(weekday == "Thursday"):
#                       if(weekday == "Friday"):
#                       if(weekday == "Saturday"):
#                       if(weekday == "Sunday"):
#                   if(info["Jour"] == "Jeudi"):
#                       if(weekday == "Monday"):
#                       if(weekday == "Tuesday"):
#                       if(weekday == "Wednesday"):
#                       if(weekday == "Thursday"):
#                       if(weekday == "Friday"):
#                       if(weekday == "Saturday"):
#                       if(weekday == "Sunday"):
#                   if(info["Jour"] == "Vendredi"):
#                       if(weekday == "Monday"):
#                       if(weekday == "Tuesday"):
#                       if(weekday == "Wednesday"):
#                       if(weekday == "Thursday"):
#                       if(weekday == "Friday"):
#                       if(weekday == "Saturday"):
#                       if(weekday == "Sunday"):
#                   if(info["Jour"] == "Samedi"):
#                       if(weekday == "Monday"):
#                       if(weekday == "Tuesday"):
#                       if(weekday == "Wednesday"):
#                       if(weekday == "Thursday"):
#                       if(weekday == "Friday"):
#                       if(weekday == "Saturday"):
#                       if(weekday == "Sunday"):
#                   if(info["Jour"] == "Dimanche"):
#                       if(weekday == "Monday"):
#                       if(weekday == "Tuesday"):
#                       if(weekday == "Wednesday"):
#                       if(weekday == "Thursday"):
#                       if(weekday == "Friday"):
#                       if(weekday == "Saturday"):
#                       if(weekday == "Sunday"):
#
#           if(info["Periode"] == "Automne"):
#               if(info.get("Jour") != None):
#                   if(info["Jour"] == "Lundi"):
#                       if(weekday == "Monday"):
#                           event.begin = ddmmyy[2] + "-" +                            
#                       if(weekday == "Tuesday"):
#                       if(weekday == "Wednesday"):
#                       if(weekday == "Thursday"):
#                       if(weekday == "Friday"):
#                       if(weekday == "Saturday"):
#                       if(weekday == "Sunday"):
#
#                   if(info["Jour"] == "Mardi"):                       
#                       if(weekday == "Monday"):
#                       if(weekday == "Tuesday"):
#                       if(weekday == "Wednesday"):
#                       if(weekday == "Thursday"):
#                       if(weekday == "Friday"):
#                       if(weekday == "Saturday"):
#                       if(weekday == "Sunday"):
#                   if(info["Jour"] == "Mercredi"):
#                       if(weekday == "Monday"):
#                       if(weekday == "Tuesday"):
#                       if(weekday == "Wednesday"):
#                       if(weekday == "Thursday"):
#                       if(weekday == "Friday"):
#                       if(weekday == "Saturday"):
#                       if(weekday == "Sunday"):
#                   if(info["Jour"] == "Jeudi"):
#                       if(weekday == "Monday"):
#                       if(weekday == "Tuesday"):
#                       if(weekday == "Wednesday"):
#                       if(weekday == "Thursday"):
#                       if(weekday == "Friday"):
#                       if(weekday == "Saturday"):
#                       if(weekday == "Sunday"):
#                   if(info["Jour"] == "Vendredi"):
#                       if(weekday == "Monday"):
#                       if(weekday == "Tuesday"):
#                       if(weekday == "Wednesday"):
#                       if(weekday == "Thursday"):
#                       if(weekday == "Friday"):
#                       if(weekday == "Saturday"):
#                       if(weekday == "Sunday"):
#                   if(info["Jour"] == "Samedi"):
#                       if(weekday == "Monday"):
#                       if(weekday == "Tuesday"):
#                       if(weekday == "Wednesday"):
#                       if(weekday == "Thursday"):
#                       if(weekday == "Friday"):
#                       if(weekday == "Saturday"):
#                       if(weekday == "Sunday"):
#                   if(info["Jour"] == "Dimanche"):
#                       if(weekday == "Monday"):
#                       if(weekday == "Tuesday"):
#                       if(weekday == "Wednesday"):
#                       if(weekday == "Thursday"):
#                       if(weekday == "Friday"):
#                       if(weekday == "Saturday"):
#                       if(weekday == "Sunday"):

#API = Api(app)

#API.app.config['RESTFUL_JSON'] = {
#    'ensure_ascii': False
#}

#from flask import request
#from flask_restful import Resource, Api
#from io import StringIO
#import simplejson as json