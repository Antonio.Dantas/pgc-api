import requests
import json

#Changer le format pour que ce soit courses{}
#Ajouter schedule text et parser
#Test if value associated with key is Null
#Create one object and then add it only once at the end

rawData = requests.get("https://pgc.unige.ch/main/api/teachings/find?academicalYear=2022&page=0&size=5000")
#modify here so that it has {"courses": [ at the beginning
rawDataJson = rawData.json()
for course in rawDataJson["_data"]:
    entityID = course["entityId"]
    NewObj = {"entityId": entityID}
    rawData2 = requests.get("https://pgc.unige.ch/main/api/teachings/" + entityID)
    rawDataJson2 = rawData2.json()
    for key, value in rawDataJson2.items():
        if (key == "academicalYear"):
            NewObj[key] = value
        if (key == "code"):
            NewObj[key] = value
        if (key == "title"):
            NewObj[key] = value
        if (key == "facultyId"):
            NewObj[key] = value
        if (key == "facultyLabel"):
            NewObj[key] = value
        if (key == "studyLevel"):
            NewObj[key] = value
        if (key == "credits"):
            NewObj[key] = value
        if (key == "activities"):
            for el in value:
                for k, v in el.items():
                    if (k == "type"):
                        NewObj[k] = v
                    if (k == "duration"):
                        NewObj[k] = v                        
                    if (k == "periodicity"):
                        NewObj[k] = v  
                    if (k == "objective"):
                        NewObj[k] = v  
                    if (k == "description"):
                        NewObj[k] = v                                                                                     
                    if (k == "codeLanguage"):
                        NewObj[k] = v  
                    if (k == "language"):
                        NewObj[k] = v   
                    if (k == "courses"):
                        for val in v:
                            for k2, v2 in val.items():
                                if (k2 == "day"):
                                    NewObj[k2] = v2
                                if (k2 == "startHour"):
                                    NewObj[k2] = v2                                    
                                if (k2 == "endHour"):
                                    NewObj[k2] = v2                                    
                                if (k2 == "frequency"):
                                    NewObj[k2] = v2
                                if (k2 == "building"):
                                    NewObj[k2] = v2
                                if (k2 == "room"):
                                    NewObj[k2] = v2
                    if (k == "activityTeachers"):
                        for val in v:
                            for k2, v2 in val.items():                            
                                if (k2 == "personId"):
                                    NewObj[k2] = v2
                                if (k2 == "teacherId"):
                                    NewObj[k2] = v2                                    
                                if (k2 == "displayFirstName"):
                                    NewObj[k2] = v2                                    
                                if (k2 == "displayLastName"):
                                    NewObj[k2] = v2
                                if (k2 == "displayFonction"):
                                    NewObj[k2] = v2
        if (key == "listStudyPlan"):
            NewObj["listStudyPlan"] = []
            for el in value:
                studyPlan = {}
                for k, v in el.items():
                    if (k == "planCredits"):
                        studyPlan[k] = v
                    if (k == "studyPlanGroupId"):
                        studyPlan[k] = v                     
                    if (k == "studyPlanPath"):
                        studyPlan[k] = v  
                    if (k == "studyPlanId"):
                        studyPlan[k] = v
                    if (k == "studyPlanLabel"):
                        studyPlan[k] = v
                    if (k == "facultyId"):
                        studyPlan[k] = v
                NewObj["listStudyPlan"].append(studyPlan)

    with open("courses.json", "r+", encoding="utf8") as outfile :
        #Load the existing data into a dictionary
        filedata = json.load(outfile)
        #Inserting the new data into the dictionary
        filedata.append(NewObj)
        #Settting the file's pointer current position back to the beginning to overwrite the previous content
        outfile.seek(0)
        #Converting the dictionary's python data back to JSON and writing into the JSON file
        json.dump(filedata, outfile, ensure_ascii=False)