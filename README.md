# API Programme des cours UNIGE

## Install

```sh
pip install virtualenv
virtualenv venv
source venv/bin/activate
pip install -r requirements.txt
python3 pgc.py
```

## Access

Go to [browser](http://127.0.0.1:5000)

## Usage 

- [/endpoint](http://127.0.0.1:5000/endpoint) - Description